---
theme: ./theme
highlighter: shiki
lineNumbers: false
website: IWCC 2021-11-07
handle: A.I. (yheuhtozr)
drawings:
  persist: false
layout: cover
title: 創作言語のための日本語文法
---

# 創作言語のための日本語文法

A.I. ([yheuhtozr.gitlab.io](https://yheuhtozr.gitlab.io))

第一回 秋季 想像世界創作会議  
2021年11月7日

<style>
  * { font-weight: bold; }
</style>

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---

# 文法、何を立てる？

<br>
<br>
<br>
<br>

<dl>
<div>
  <dt>性</dt><dd>男性・女性・中性…</dd>
</div>
<div>
  <dt>数</dt><dd>単数・複数・双数…</dd>
</div>
<div>
  <dt>格</dt><dd>主格・対格・与格…</dd>
</div>
<div>
  <dt>人称</dt><dd>一人称・二人称…</dd>
</div>
<div>
  <dt>時制</dt><dd>過去・現在・未来…</dd>
</div>
<div>
  <dt>法</dt><dd>直説法・仮定法・命令法…</dd>
</div>
</dl>

<!--
You can have `style` tag in markdown to override the style for the current page.
Learn more: https://sli.dev/guide/syntax#embedded-styles
-->

<style>
  dl { @apply grid grid-cols-3 gap-4 m-4; }
  dl div { @apply bg-emerald-300 rounded-lg p-1; }
  dt { @apply block m-1 text-center; }
  dd { @apply block bg-light-50 rounded-lg p-2 m-2; }
</style>

---

# 日本語にある文法

<dl>
<div class="none">
  <dt>性</dt><dd>男性・女性・中性…</dd>
</div>
<div class="none">
  <dt>数</dt><dd>単数・複数・双数…</dd>
</div>
<div>
  <dt>格</dt><dd>主格・対格・与格…</dd>
</div>
<div class="none">
  <dt>人称</dt><dd>一人称・二人称…</dd>
</div>
<div>
  <dt>時制</dt><dd>過去・現在・<s>未来</s>…</dd>
</div>
<div>
  <dt>法</dt><dd>直説法・<s>仮定法</s>・命令法…</dd>
</div>
</dl>

<dl class="new" v-click>
<div><dt>有生性</dt><dd>ある・いる</dd></div>
<div><dt>主観性（？）</dt><dd>たい・たがる…</dd></div>
<div><dt>方向性（？）</dt><dd>あげる・もらう…</dd></div>
<div><dt>証拠性</dt><dd>らしい・そうだ・ようだ…</dd></div>
<div><dt>類別詞</dt><dd>個・本・匹…</dd></div>
</dl>


<style>
  s { @apply text-gray-400 font-bold; }
  dl { @apply grid grid-cols-3 gap-4 m-4; }
  dl div { @apply bg-emerald-300 rounded-lg p-1; }
  dl.new div { @apply bg-pink-300; }
  div.none { @apply bg-gray-200 text-gray-400; }
  dt { @apply block m-1 text-center; }
  dd { @apply block bg-light-50 rounded-lg p-2 m-2; }
</style>

---

# 有生性

意志や自発的な動きを持つかどうかの区分。標準日本語では**有生**・**無生**で二分される。

<div grid="~ cols-2 gap-2">
<div>

### 標準日本語

動詞と動作主の適格性が変化

- 弟／幽霊／ゾンビが**いる**。（有生）
- キノコ／鉛筆／金が**ある**。（無生）
- Alexa／ルンバ／タクシーが**いる**？**ある**？

<p><hr></p>

- 風**で**窓**が**開いた。
- ?? 風**が**窓**を**開いた。

</div>
<div v-click="1">

### 喜界方言（上嘉鉄）

属格標識「～の」が有生性の度合いによって変化  
<small>（[重野・白田 2016](http://harp.lib.hiroshima-u.ac.jp/hue/metadata/12233)）</small>

- ***waa*** mun（私のもの）
- an=***ŋa*** k’uruma（彼の車）
- da-nnaa yaa（お前たちの家）
- haku=***nu*** naa（箱の中）

<p class="block w-full text-center" v-click="2"><strong>有生性階層</strong></p>

</div>
</div>

---

# 有生性階層

有生性の区分がより複雑な言語もある。

### ナバホ語

大人 > 子ども・大型動物 > 中型動物 > 小型動物 > 自然現象 > 抽象概念

低い階層の語を動作主にとれない

<div grid="~ cols-2 gap-2">
<div>
<div interlinear>
<p class="gloss">
  <div class="gll">Ashkii<br>少年</div>
  <div class="gll">at’ééd<br>少女</div>
  <div class="gll">yiníł’į́<br>yi-見る</div>
</p>
<p class="glt">「少年が少女を見ている」</p>
</div>

<div interlinear>
<p class="gloss">
  <div class="gll">At’ééd<br>少女</div>
  <div class="gll">ashkii<br>少年</div>
  <div class="gll">biníł’į́<br>bi-見る</div>
</p>
<p class="glt">「少女が少年に見られている」</p>
</div>
</div>

<div>
<div interlinear>
<p class="gloss">
  <div class="gll">Tsídii<br>鳥</div>
  <div class="gll">at’ééd<br>少女</div>
  <div class="gll">yishtąsh<br>yi-つつく</div>
</p>
<p class="glt">❌「鳥が少女をつついている」</p>
</div>

<div interlinear>
<p class="gloss">
  <div class="gll">At’ééd<br>少女</div>
  <div class="gll">tsídii<br>鳥</div>
  <div class="gll">bishtąsh<br>bi-つつく</div>
</p>
<p class="glt">「少女が鳥につつかれている」</p>
</div>
</div>
</div>

---
layout: image-right
image: Man-Bites-Dog.jpg
---

# 有生性の応用

- 例えば、**格**の代わりに有生性に一致する要素で意味関係を標示する
- あるいは、**態**の代わりに有生性階層に対する方向性を標示する（順行・逆行態）
- **話題性階層**（topicality hierarchy）とつなげてその言語らしい言い回しの一部に
- ちなみに、印欧語の**中性名詞**は本来無生物名詞が起源とされる

---

# 主観性

という用語は確立していませんが…。

<div grid="~ cols-2 gap-2">
<div>

### 英語

- I **want** to watch a movie.
- You **want** to watch a movie.
- She **wants** to watch a movie.

</div>
<div>

### 日本語

- 私は映画を見**たい**。
- <span v-click="1">あなたは映画を見</span><span v-click="2">**たがっている**。</span>
- <span v-click="3">彼女は映画を見</span><span v-click="4">**たがっている**。</span>

<div v-click="5">
<p><hr></p>

- あなたは映画を見**たい**ですか？
- 彼は映画を見**たい**らしい。

<p v-click="6">視点を置いている人以外には心理的形容詞を適用できない</p>
<p class="block w-full text-center" v-click="7"><strong>私以外私じゃないの</strong></p>

</div>
</div>
</div>

---

# 方向性

という用語もないですが…。

- プレゼントを**あげる**。（私（たち）から誰かへ）
- プレゼントを**くれる**。（誰かから私（たち）へ）

<p><hr></p>

- 殴って**やった**。
- 殴って**きた**。

<v-click>

しくみとしては「**行く**」↔「**来る**」の対立と同じようなもの

**私**（話し手）からの遠近による階層化（共感度階層<small>（久野 1978）</small>）

</v-click>

---

# 主観性／方向性の応用

行為が起きている環境を指し示すストラテジー

<div grid="~ cols-2 gap-2">
<div>

### 英語式

![英語模式図](/english-strategy.svg)

<p class="block w-full text-center">主に項の「位置」を明示する</p>

</div>
<div>

### 日本語式

![日本語模式図](/japanese-strategy.svg)

<p class="block w-full text-center">主に述語の「ベクトル」を明示する</p>

</div>
</div>

---

# 証拠性

ソースどこよ？

<div grid="~ cols-2 gap-2">
<div>

### 日本語

- 雨が降る
- 雨が降**りそうだ**
- 雨が降**るそうだ**
- 雨が降る**ようだ**
- 雨が降る**らしい**

</div>
<div v-click>

### Eastern Pomo <small>([WP](https://en.wikipedia.org/wiki/Evidentiality))</small>

情報源|語形|意味
:-:|:-|:-
感覚|pʰa·békʰ-**ink’e**|焼けた（と感じる）
推量|pʰa·bék-**ine**|焼けた（と思える）
伝聞|pʰa·békʰ-**·le**|焼けたらしい
一次知識|pʰa·bék-**a**|焼けた ~~（確信）~~

証拠性が**必須**な言語もある

</div>
</div>

---

# 証拠性の分布

日本語は**間接**知覚のみ（ピンク）、**直接**知覚にも適用する言語もある（赤）<small>（図：[WALS](https://wals.info/feature/77A)）</small>

![証拠性の地図](/evidentiality.png)

---
layout: items
---

# 類別詞

日本語では「助数詞」のこと

::items::

<div class="object" v-click="1"><img src="/apple-tree-svgrepo-com.svg"></div>
<div class="object" v-click="2"><img src="/apple-svgrepo-com.svg"></div>
<div class="object" v-click="3"><img src="/box-cardboard-svgrepo-com.svg"></div>
<div class="object" v-click="4"><img src="/apple-logo-svgrepo-com.svg"></div>

<p v-click="1">一<strong>本</strong></p>

<p v-click="2">一<strong>個</strong></p>

<p v-click="3">一<strong>箱</strong></p>

<p v-click="4">一<strong>社</strong></p>

<style>
  .object { margin: auto; }
  img { width: 5em; height: 5em; }
  .items-grid p { margin-top: 1em; font-size: 2em; }
</style>

---

# 類別詞の働き

<div grid="~ cols-2 gap-2">
<div>

可算名詞・不可算名詞・集合名詞などの区別（場合によっては単複なども）を不要にする

- 考え方によってはすべて不可算名詞
  - a **cup** of water
  - 一**杯**の水

</div>
<div v-click>

文法性（名詞クラス）の代わりに名詞の抽象的区分としての役割を果たす

- **一本**とられた
- **一発**当てる
- **一丁**あがり

下につく具体的な名詞はもはや存在しない…？

</div>
</div>

---

# 名詞を区分するカテゴリ

<div grid="~ cols-3 gap-2">
<div>

### 類別詞

数詞が変化

- アジア一帯に多い
- 南米先住民語にも広く分布<small>（図：[WALS](https://wals.info/feature/55A)）</small>

</div>
<div>

### 類別動詞

動詞が変化

- 北米先住民語のみ（？）

</div>
<div>

### 名詞クラス

主に連体詞が変化

- ヨーロッパ～アフリカに多い
- 極端に抽象化された形が**性**ともいえる<small>（図：[WALS](https://wals.info/feature/30A)）</small>

</div>
</div>

<div grid="~ cols-2 gap-2">

![類別詞の地図](/numeral-classifiers.png)

![名詞クラスの地図](/genders.png)

</div>

<style>
  h3, h3 + p { text-align: center; }
</style>

---

# まとめ（雑）

- よく知られている文法カテゴリはぶっちゃけラテン語文法の焼き直し
- 日本語の文法現象は研究の歴史が比較的浅い
  - 知名度がないか、整理が進んでいないテーマはいっぱいある
  - 実は日本語特有ではない（通言語的に一般化できる）文法項目も結構ある
  - **主題**（情報構造）・**助詞**（接語）・**敬語**（待遇表現・ポライトネス）・**終助詞**（対人モダリティ）・**接辞スロット**・**複合動詞**・**名詞述語構文**・**意外性**・**擬態語**（ideophone）・**位相語**…
- 世界の言語はどんな性質を文法として抽出しているのか（類型論・対照言語学）？
  - [WALS](https://wals.info/)
  - 『言語類型論入門』（リンゼイ 2006）
  - 『言語が違えば、世界も違って見えるわけ』（ドイッチャー 2012）

---
layout: statement
---

# ご清聴ありがとうございました

A.I.  
[yheuhtozr.gitlab.io](https://yheuhtozr.gitlab.io)  
<logos-gitlab /> [yheuhtozr](https://gitlab.com/yheuhtozr)  
<logos-twitter /> [Wartemeinnicht](https://twitter.com/Wartemeinnicht)

<style>
  h1 + p { margin-top: 1em; }
</style>

---